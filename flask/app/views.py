from crypt import methods
from email import message
from logging.config import valid_ident
from termios import VLNEXT
from app import app
from flask import Flask, render_template, request
import mysql.connector
import os


# getting Env variables
os.environ['DB_URL'] = 'mysql-service' 
DB_URL = os.getenv('DB_URL')

os.environ['MYSQL_USER'] = 'navraja'
MYSQL_USER = os.getenv('MYSQL_USER')

MYSQL_PASSWORD = os.getenv('MYSQL_PASSWORD')

# Database connection
def getMysqlConnection():
    return mysql.connector.connect(
        user = MYSQL_USER,
        host = DB_URL,
        port = '3306',
        password = MYSQL_PASSWORD,
        database = 'firstdb'
    )
db = getMysqlConnection()
mycursor = db.cursor()

# defining routes

@app.route('/', methods=['GET', 'POST'])
def handle_data():
    if request.method == "POST":
        username = request.form['username']
        email = request.form['email']
        subject = request.form['subject']
        message = request.form['message']
        sql = "INSERT INTO message (username, email, subject, message) VALUES (%s, %s, %s, %s)"
        val = (username, email,subject, message]
        mycursor.execute(sql, val)
        db.commit()
    else: 
        return render_template('index.html')

@app.route('/homelab')
def homelab():
    return render_template('homelab.html')

@app.route('/networkdesign')
def networkdesign():
    return render_template('networkdesign.html')
    
if __name__ == '__main__':
    app.run(debug=True)
